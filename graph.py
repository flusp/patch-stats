from bs4 import BeautifulSoup as bs
import urllib.request as request
import matplotlib.pyplot as plt
from datetime import datetime
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates


url = "https://patchwork.kernel.org/project/linux-iio/list/?submitter="
ids = [182775, 182929, 182787, 182923, 183037, 182791]
labels = {}


dates = []

for id in ids:
	soup = bs(request.urlopen(url + str(id)), features="lxml")
	table = soup.find('table', {'id': 'patchlist'})
	a = table.tbody.findAll("tr")
	for row in a:
		b = row.findAll("td")
		# print("----------")
		text = b[4].get_text()
		# labels[b[5].get_text()] = 1
		# dates.append(text)
		text = text.split("-")
		dates.append(datetime(int(text[0]),int(text[1]),int(text[2]),0,0))


count = Counter(dates)
labels = list(labels.keys())
print(labels)

x = []
y = []

k = [i for i in count.keys()]
k.sort()

old = 0

x.append(datetime(2018,7,1,0,0))
y.append(old)

for i in k:
	old += count[i]
	x.append(i)
	y.append(old)



years = mdates.YearLocator()   # every year
months = mdates.MonthLocator()  # every month
monthsFmt = mdates.DateFormatter('%Y-%m-%d')

fig, ax = plt.subplots(figsize=(18, 10), dpi=450)
ax.stackplot(x, y, labels=labels)
ax.legend(loc='upper left')


# format the ticks
ax.xaxis.set_major_locator(months)
ax.xaxis.set_major_formatter(monthsFmt)
# ax.xaxis.set_minor_locator(months)

# round to nearest years...
# datemin = np.datetime64(x[0], 'Y')
# datemax = np.datetime64(x[-1], 'Y') + np.timedelta64(1, 'Y')
# ax.set_xlim(datemin, datemax)


# format the coords message box
ax.format_xdata = mdates.DateFormatter('%Y-%m-%d')
ax.grid(True)

# rotates and right aligns the x labels, and moves the bottom of the
# axes up to make room for them
fig.autofmt_xdate()

# plt.show()

plt.savefig('to.png')   # save the figure to file
