import re
import urllib.request as request
import matplotlib.pyplot as plt

def getCommitCount(msg, users):
    dic = {}
    reg = '|'.join(users)
    p = re.compile("(%s) \(([0-9]+)\)" % reg)
    soma = 0
    for x in p.findall(msg):
        dic[x[0]] = int(x[1])
    return dic

def getTotalCommitCount(msg):
    p = re.compile("\(([0-9]+)\)")
    soma = 0
    for x in p.findall(msg):
        #print(x)
        soma += int(x)
    return soma

url = "https://www.spinics.net/lists/linux-iio/msg43827.html"

uf = request.urlopen(url)
html = str(uf.read())
 
users = ["Anderson Reis", "Fernandes", "Camylla Cantanheide", "Giuliano Belinassi", "Lucas Oshiro", "Marcelo Schmitt", "Matheus Tavares", "Renato Lui Geh", "Rodrigo Ribeiro"]

group = getCommitCount(html, users)

### gambiarra a gente aceita, o que a gente não aceita é a derrota =p
bárbara = group['Fernandes']
del group['Fernandes']
group['Bárbara Fernandes'] = bárbara
###

total = getTotalCommitCount(html)

group['Others'] = total - sum([p[1] for p in group.items()])

# Pie chart, where the slices will be ordered and plotted counter-clockwise:
labels = tuple([p[0] for p in group.items()])
sizes = [p[1] for p in group.items()]
explode = tuple([.3 for p in group.items()])

fig1, ax1 = plt.subplots()
ax1.pie(sizes, explode=explode, autopct='%1.1f%%', startangle=90)
plt.legend(labels=labels)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

plt.show()
